# Proyek Pekan RISTEK 

**Tutorial cara menjalankan Aplikasi di lokal**
1. lakukan perintah `git clone` dengan URL repository ini untuk menyimpannya dalam suatu folder pada PC/Laptop anda.
2. Setelah di-clone, buka folder tersebut pada IDE favoritmu (Visual Studio Code, IntelliJ, Jupyter, dll)
3. Buka terminal Git Bash lalu jalankan keempat perintah ini:
`python -m venv env`
untuk menyalakan Python virtual environment.

`source env/Scripts/activate`
untuk memasuki Python virtual environment pada terminal.

`pip install uvicorn`
untuk meng-install web server yang dapat menjalankan aplikasi FastAPI.

`uvicorn main:app --reload`
untuk membuka aplikasi kita pada mesin lokal.