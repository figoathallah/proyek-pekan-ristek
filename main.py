from datetime import datetime
from fastapi import FastAPI, Request
from fastapi.staticfiles import StaticFiles
from fastapi.responses import HTMLResponse
from pydantic import BaseModel
from fastapi.templating import Jinja2Templates
from pathlib import Path
import models
from database import sessionLocal

app = FastAPI()

app.mount(
    "/static",
    StaticFiles(directory=Path(__file__).parent.parent.absolute() / "static"),
    name="static",
)

templates = Jinja2Templates(directory="templates")

class Savings(BaseModel):
    id:int
    date:datetime
    description:str
    incomeAmount:int
    expenseAmount:int

    class Configure:
        orm_mode=True

db = sessionLocal()


@app.get('/', response_class=HTMLResponse)
def index(request: Request):
    return templates.TemplateResponse("index.html", {"request": request})

@app.get('/savings', response_class=HTMLResponse)
def viewSavings(request: Request, savings: list):
    savings = db.query(models.Savings).all()
    return templates.TemplateResponse("savings.html", {"request": request, "savings": savings})

@app.post('/savings')
def createLog():
    pass

@app.put('/savings/update-log/{savings_id}')
def updateLog(savings_id:int):
    pass

@app.delete('/savings/delete-log/{savings_id}')
def deleteLog(savings_id:int):
    pass

@app.get('/add-log', response_class=HTMLResponse)
def viewCreateLog(request: Request):
    return templates.TemplateResponse("add_log.html", {"request": request})

@app.get('/delete-log', response_class=HTMLResponse)
def viewDeleteLog(request: Request, id: int):
    return templates.TemplateResponse("delete_log.html", {"request": request, "id": id})
    
@app.get('/update-log', response_class=HTMLResponse)
def viewUpdateLog(request: Request, id: int):
    return templates.TemplateResponse("update_log.html", {"request": request, "id": id})