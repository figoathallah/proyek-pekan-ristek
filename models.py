from database import base 
from sqlalchemy import Text, Integer, DateTime, Column
from sqlalchemy.sql import func

class Savings(base):
    __tablename__ ='savings'
    id = Column(Integer, primary_key=True)
    date = Column(DateTime(timezone=True), server_default=func.now())
    description = Column(Text)
    incomeAmount = Column(Integer)
    expenseAmount = Column(Integer)

    def __repr__(self):
        return f"{self.description}"
